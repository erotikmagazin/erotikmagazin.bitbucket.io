// Create a HTML div Element called 'page'
// var page = document.createElement('DIV');
var magazinObj = document.getElementById("Cover1");
var page = document.getElementById("background");
// Gives the page variable full height 
page.style.height = '100vh';

// Applies the page element to the document(web page)
document.body.appendChild(page);

//Creates variables for x & y for know where our mouse is
//x is for horizontal values, and y for vertical ones
var x = 0;
var y = 0;
const maxX = document.body.clientWidth;
const maxY = document.body.clientHeight;
//console.log("maxx: "+maxX+" maxy: "+maxY);

// Add Event Listener for page. Listens for any mouse movement
// inside the page element. If found, run function below
document.addEventListener('mousemove', function (event) {

    //Takes the mouse movement we listened for and saves it into two variables
    x = event.clientX;
    y = event.clientY;
    var xVal = (x / maxX) * 255;
    var yVal = (y / maxY) * 255;
    //console.log("x: "+xVal+" y: "+yVal);

    //Here we set the background color to the x & y value that the mouse has over the web page. See css part for rgb explaination
    page.style.backgroundColor = 'rgb(' + xVal + ', ' + yVal + ', 255)';
    //By writing variable + ', ' we combine the value with text to make it write like rgb(x, y, 100); when sent to style part (css)
    //Adds a text element to the page. It writes out the x & y value

});

var circles = document.getElementsByClassName("circle")

var i = 0
for (var circle of circles) {
    circle.style.left = 40 + i * 10 + "vw"
    circle.style.top = "5vh";
    i++;
}

console.log(circles)
var shell = document.getElementById("shell");
var randomSize = Math.random() * 10 + 15;
shell.style.width = randomSize + "vw";
shell.style.height = randomSize + "vw";
shell.style.borderRadius = randomSize + "vw";

var randomTop = Math.random() * 60;
var randomLeft = Math.random() * 80;
shell.style.top = randomTop + "vh";
shell.style.left = randomLeft + "vw";

var shelllayer = document.getElementById("shelllayer");
shelllayer.style.top = shell.style.top
shelllayer.style.left = shell.style.left
shelllayer.style.width = shell.style.width
shelllayer.style.height = shell.style.height
var mouseover = false

shelllayer.addEventListener('mouseover', function (event) {
    // circle.style.visibility = "visible"
    // circle.style.opacity = 1
    if (!mouseover) {
        shell.style.filter = "blur(20px)";
        shell.style.transition = "5s";
        mouseover = true
        console.log("mouseover true")
        for (var circle of circles) {
            console.log(circle)
            circle.style.animation = "fadeIN 5s ";
        }
        setTimeout(function () {
            for (var circle of circles) {
                console.log("faded in")
                circle.style.opacity = 1
                circle.style.animation = "pulseC1 10s infinite"
            }
        }, 5000)

    }
});

shelllayer.addEventListener('mouseout', function (event) {


    setTimeout(function () {
        shell.style.filter = "blur(0px)";
        shell.style.transition = "5s";
        for (var circle of circles) {
            circle.style.animation = "fadeIN 5s reverse"
        }
        setTimeout(function () {
            closemenu()
        }, 5000)
    }, 5000)
});

function closemenu() {
    for (var circle of circles) {
        circle.style.animation = ""
        circle.style.opacity = 0;
    }
    console.log("mouseover false")
    mouseover = false

}

// $("#textfield").hide();
// $("#about").click(function () {
//     document.getElementById("textfield").InnerHTML = '<object type="text/html" data="pages/test.html" ></object>';
//     setTimeout(function () {
//         // $("#textfield").show();

//         $('.textfield').addClass('textfieldShow')

//     }, 800)

// });

$('.circle').on('click', function () {
    // console.log("click")
    // $('.testViereck').removeClass('testViereckSelectedAlt');
    $(this).addClass('circleSelected')
    setTimeout(function () {
        console.log("end")
        $('.circle').removeClass('circleSelected')
    }, 500)
});


// $(function () {
//     $('#magazin').hover(function () {
//         $('#Cover1').fadeIn();
//         $('#Cover2').fadeIn();
//     }, function () {
//         $('#Cover1').fadeOut(5000);
//         $('#Cover2').fadeOut(5000);
//     });
// });

$('#magazin').mouseover(function () {
    if (mouseover) {
        console.log("hover in magazin");
        // var Cover1 = document.getElementById("Cover1");
        // var Cover2 = document.getElementById("Cover2");
        // Cover1.fadeIn();
        // Cover2.fadeIn();
        $('#Cover1').fadeIn(1);
        $('#Cover2').fadeIn(1);
    }
});

$('#magazin').mouseout(function () {
    console.log("hover out magazin");
    setTimeout(()=>{
        $('#Cover1').fadeOut(5000);
        $('#Cover2').fadeOut(5000);
    }, 5000)
});


//crawl 60s linear
$('#about').mouseover(function () {
    if (mouseover) {
        console.log("hover in about");
        var crawl = document.getElementById("crawl");
        crawl.style.display = "block";
        crawl.style.animation = "crawl 120s linear";
    }

});
$('#about').mouseout(function () {
    console.log("hover out about");
    var crawl = document.getElementById("crawl");
    crawl.style.animation = "none";
    crawl.style.display = "none";
});





// $('#magazin').mouseover(function () {
//     console.log("hover in magazin");
//     var Cover1 = document.getElementById("Cover1");
//     var Cover2 = document.getElementById("Cover2");
//     Cover1.style.display="block";
//     Cover2.style.display="block";
//     // $('#Cover1').show();
//     // $('#Cover2').show();
// });

// $('#magazin').mouseout(function () {
//     console.log("hover out magazin");
//     var Cover1 = document.getElementById("Cover1");
//     var Cover2 = document.getElementById("Cover2");
//     Cover1.style.display="none";
//     Cover2.style.display="none";
//     // $('#Cover1').hide();
//     // $('#Cover2').hide();
// });


window.onload = function () {


    // Create the renderer and add it to the page's body element
    var renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true, autoSize: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    magazinObj.appendChild(renderer.domElement);

    // Create the scene to hold the object
    var scene = new THREE.Scene();

    // Create the camera
    var camera = new THREE.PerspectiveCamera(
        35,                                     // Field of view
        window.innerWidth / window.innerHeight, // Aspect ratio
        0.1,                                    // Near plane distance
        1000                                    // Far plane distance
    );

    // Position the camera
    // camera.position.set(-15, 10, 20);
    camera.position.set(0, 20, 80);


    // Add the lights

    var light = new THREE.PointLight(0xffffff, .4);
    light.position.set(10, 10, 10);
    scene.add(light);

    ambientLight = new THREE.AmbientLight(0xbbbbbb);
    scene.add(ambientLight);


    // Load the textures (book images)
    var textureLoader = new THREE.TextureLoader();
    var mag1CoverTexture = textureLoader.load('./Bilder/Magazin_final_webshow.jpg');
    var mag1SpineTexture = textureLoader.load('./Bilder/Ruecken.jpg');
    var mag1BackTexture = textureLoader.load('./Bilder/Magazin_final_webshow132.jpg');
    var mag1PagesTexture = textureLoader.load('./Bilder/southern-gems-pages.png');
    var mag1PagesTopBottomTexture = textureLoader.load('./Bilder/southern-gems-pages-topbottom.png');


    // Use the linear filter for the textures to avoid blurriness
    mag1CoverTexture.minFilter
        = mag1SpineTexture.minFilter
        = mag1BackTexture.minFilter
        = mag1PagesTexture.minFilter
        = mag1PagesTopBottomTexture.minFilter
        = THREE.LinearFilter;


    // Create the materials

    var mag1Cover = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag1CoverTexture });
    var mag1Spine = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag1SpineTexture });
    var mag1Back = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag1BackTexture });
    var mag1Pages = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag1PagesTexture });
    var mag1PagesTopBottom = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag1PagesTopBottomTexture });

    var materials = [
        mag1Pages,          // Right side
        mag1Spine,          // Left side
        mag1PagesTopBottom, // Top side
        mag1PagesTopBottom, // Bottom side
        mag1Cover,          // Front side
        mag1Back            // Back side
    ];

    // Create the mag1 and add it to the scene
    var mag1 = new THREE.Mesh(new THREE.BoxGeometry(17.5, 24, 1.2), materials);//7, 10, 1.2, 4, 4, 1
    // const mag1_scale = 0.3;
    // mag1.scale.set(mag1_scale, mag1_scale, mag1_scale);
    // mag1.position.y += window.innerHeight / 100;
    mag1.position.x += -window.innerWidth / 70;
    //mag1.position.z -= 20;
    scene.add(mag1);

    ///////////////

   // Load the textures (book images)
   var textureLoader = new THREE.TextureLoader();
   var mag2CoverTexture = textureLoader.load('./Bilder/Magazin2_einzeln/Cover2.jpg');
   var mag2SpineTexture = textureLoader.load('./Bilder/Magazin2_einzeln/Ruecken2.jpg');
   var mag2BackTexture = textureLoader.load('./Bilder/Magazin2_einzeln/backcover2.jpg');
   var mag2PagesTexture = textureLoader.load('./Bilder/southern-gems-pages.png');
   var mag2PagesTopBottomTexture = textureLoader.load('./Bilder/southern-gems-pages-topbottom.png');


   // Use the linear filter for the textures to avoid blurriness
   mag2CoverTexture.minFilter
       = mag2SpineTexture.minFilter
       = mag2BackTexture.minFilter
       = mag2PagesTexture.minFilter
       = mag2PagesTopBottomTexture.minFilter
       = THREE.LinearFilter;


   // Create the materials

   var mag2Cover = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag2CoverTexture });
   var mag2Spine = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag2SpineTexture });
   var mag2Back = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag2BackTexture });
   var mag2Pages = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag2PagesTexture });
   var mag2PagesTopBottom = new THREE.MeshLambertMaterial({ color: 0xffffff, map: mag2PagesTopBottomTexture });

   var materials = [
       mag2Pages,          // Right side
       mag2Spine,          // Left side
       mag2PagesTopBottom, // Top side
       mag2PagesTopBottom, // Bottom side
       mag2Cover,          // Front side
       mag2Back            // Back side
   ];

   // Create the mag2 and add it to the scene
   var mag2 = new THREE.Mesh(new THREE.BoxGeometry(17.5, 24, 1.2), materials);//7, 10, 1.2, 4, 4, 1
//    const mag2_scale = 0.3;
//    mag2.scale.set(mag2_scale, mag2_scale, mag2_scale);
//    mag2.position.y += window.innerHeight / 100;
   mag2.position.x += window.innerWidth / 70;
   //mag2.position.z -= 20;
   scene.add(mag2);

   mag1.rotation.x -= 0.4;
   mag2.rotation.x -= 0.4;

    //////////////

    // Create the orbit controls for the camera
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enablePan = false;
    controls.enableZoom = false;



    const mouse = new THREE.Vector2();

    // Begin the animation
    animate();

    renderer.domElement.addEventListener("click", onclick, true);
    var selectedObject;
    const raycaster = new THREE.Raycaster();

    function onclick(event) {
        var mouse = new THREE.Vector2();
        mouse.x = ((event.clientX) / window.innerWidth) * 2 - 1;// - magazinObj.offsetLeft
        mouse.y = - ((event.clientY) / window.innerHeight) * 2 + 1;// - magazinObj.offsetTop
        raycaster.setFromCamera(mouse, camera);
        var intersects = raycaster.intersectObjects([mag1, mag2]);
        if (intersects.length > 0) {
            if(intersects[0].object.uuid === mag1.uuid){
                window.location.href = "./NR1.html";
            } else if(intersects[0].object.uuid === mag2.uuid){
                window.location.href = "./NR2.html";
            }
        }
    }


    /*
      Animate a frame
    */

    function animate() {

        // Update the orbit controls
        controls.update();

        // Render the frame
        renderer.render(scene, camera);
        // console.log("animate")
        mag1.rotation.y += 0.01;
        mag2.rotation.y += 0.01;
        // Keep the animation going
        requestAnimationFrame(animate);
    }


}