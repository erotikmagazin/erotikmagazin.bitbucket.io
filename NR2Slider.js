// import "https://unpkg.com/swiper/js/swiper.min.js"

var mySwiperLinks
var mySwiperRechts
var swiperOptions = {
  updateOnWindowResize: true,
  centeredSlides:true,
  centeredSlidesBounds:true,
  autoHeight:true,
  loop: true,
  direction: 'vertical',
  slidesPerView: 1,
  mousewheel: true,
  speed: 4000,
  simulateTouch:false,
}

var active = "none"
$(document).ready(function () {
  console.log("hallo swiper")
  //initialize swiper when document ready
  mySwiperRechts = new Swiper('#rechts',swiperOptions)

  mySwiperLinks = new Swiper('#links',swiperOptions)

  mySwiperRechts.on('slideNextTransitionStart', ()=>{
    if(active!="left"){
      active = "right"
      mySwiperLinks.slidePrev()
    }
  })

  mySwiperLinks.on('slideNextTransitionStart', ()=>{
    if(active!="right"){
      active = "left"
      mySwiperRechts.slidePrev()
    }
  })

  mySwiperRechts.on('slidePrevTransitionStart', ()=>{
    if(active!="left"){
      active = "right"
      mySwiperLinks.slideNext()
    }
  })

  mySwiperLinks.on('slidePrevTransitionStart', ()=>{
    if(active!="right"){
      active = "left"
      mySwiperRechts.slideNext()
    }
  })

  mySwiperLinks.on('slideChangeTransitionEnd', ()=>{
    active = "none"
  })

  mySwiperRechts.on('slideChangeTransitionEnd', ()=>{
    active = "none"
  })

});
